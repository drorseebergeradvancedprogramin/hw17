#pragma once
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <md5.h>
#include <hex.h>
#include "CryptoDevice.h"
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include <string>
#include <thread>

#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_BUFLEN 512            
#define IP_ADDRESS "127.0.0.1"
#define DEFAULT_PORT "8202"
#define MAX_LOGIN_ATTEMPTS 4

#define LOGIN_NAME "eli"
#define LOGIN_HASH "F3AEB4D2CF0DD58C7BDA99F4E2582346"
// password is 'elibanana'


struct client_type
{
	SOCKET socket;
	int id;
	char received_message[DEFAULT_BUFLEN];
};

class Client
{
public:
	Client();
	~Client();
	int main();

private:
	int process_client(client_type  &new_client);
};

