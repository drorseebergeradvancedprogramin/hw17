#pragma once

#include <cstdio>
#include <iostream>
#include "osrng.h"
#include "des.h"
#include "modes.h"
#include <string.h>
#include <cstdlib>


using namespace std;


class CryptoDevice
{

public:
	CryptoDevice();
	~CryptoDevice();
	string encryptAES(string);
	string decryptAES(string);

};
